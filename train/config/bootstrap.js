/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 //infura
 */
var Web3 = require('web3');
var web3 = new Web3( new Web3.providers.HttpProvider("https://kovan.infura.io/FrDFhx3FbezOwQJjQv9T"));
//var web3 = new Web3( new Web3.providers.HttpProvider("https://mainnet.infura.io/ZviHOjH6h9hLmMDkU3yY"));


module.exports.bootstrap = function(cb) {

    var account =web3.eth.accounts.create();
    // console.log("account",account);

    //查询以太币的余额
    web3.eth.getBalance(account.address)
    .then((res)=>{
    	console.log("balance of 0xbb27b0B5FF81496Ea7DD5312E5eE3e2831c61034",web3.utils.fromWei(res));
    });


  //   account { address: '0xbb27b0B5FF81496Ea7DD5312E5eE3e2831c61034',
  // privateKey: '0x284ab6a4017f26ea85c185904524629d3d3b3c5254fa2a5ab8250b4a4c044602',
  // signTransaction: [Function: signTransaction],
  // sign: [Function: sign],
  // encrypt: [Function: encrypt] }



  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
